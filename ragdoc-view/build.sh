#!/bin/bash

set -eu

if [ ! -d 'rd-builder' ] || [ ! -e "rd-builder/build.gradle" ]; then
  echo 'Pulling RD-Builder'
  git submodule init
fi
git submodule update

EXTENDJ="rd-builder/extendj"

echo "ExtendJ path: $EXTENDJ"
if [ ! -d '$EXTENDJ' ] || [ ! -e "$EXTENDJ/build.gradle" ]; then
  echo 'Pulling ExtendJ'
  (cd rd-builder; git submodule init)
fi
(cd rd-builder; git submodule update)

# Optional debug flags.
DEBUG="${DEBUG:-}"
#DEBUG="-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005"

(cd rd-builder; ./gradlew fatJar)

(cd $EXTENDJ; ./gradlew :java8:jar)

java \
  $DEBUG \
  -jar rd-builder/rd-builder.jar \
  -d src/data \
  -ragroot $EXTENDJ \
  $(find $EXTENDJ/src -name '*.java') \
  $(find $EXTENDJ/java8/src -name '*.java')

# Build packed assets for publishing.
ng build --prod --base-href "/doc/"
