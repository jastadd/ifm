import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class SourceService {

  constructor(private http: Http) { }

  getSource(filename: String): Promise<string> {
    return this.http.get(`data/${filename}`)
        .toPromise()
        .then(response => response.text())
        .catch(res => Promise.reject(`Failed to load source: ${name}: ${res}.`));
  }
}
