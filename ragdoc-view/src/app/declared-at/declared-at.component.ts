import { Component, Input } from '@angular/core';

import {Doc} from '../doc';

@Component({
  selector: 'declared-at',
  template: `
    Declared at <a [routerLink]="['/source', filename, line]">{{filepath}}:{{line}}.</a>
  `,
})
export class DeclaredAtComponent {

  private _doc: Doc;
  filename: string;
  filepath: string;
  line: string;

  constructor() { }

  @Input()
  set doc(doc: Doc) {
    this._doc = doc;
    this.filepath = doc.ragFile;
    this.filename = this.filepath.replace(/\/|\\|\./g, '_');
    this.line = String(doc.line);
  }
}
