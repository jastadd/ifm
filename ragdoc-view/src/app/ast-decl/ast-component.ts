import {TypeRef} from '../type-ref';

export class AstComponent {
  name: string;
  type: TypeRef;
  kind: string;

  static fromJson(json: any): AstComponent {
    var name: string = undefined;
    if (json.n) {
      name = json.n as string;
    }
    var kind = "regular";
    if (json.k) {
      kind = json.k;
    }
    return {
      name: json.n as string,
      type: TypeRef.fromJson(json.e),
      kind: kind,
    };
  }
}
