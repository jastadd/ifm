aspect ConnectedComponents {

  /**
   * Kosaraju's algorithm
   */
  syn Set<Set<State>> StateMachine.SCC() {
    Map<State, Set> visited = new HashMap<>();
    LinkedList<State> locked = new LinkedList<>();

    for (State n : states())
      if (!visited.containsKey(n))
        n.visit(visited, locked);              // forward search

    for (State n : locked)
      if (visited.get(n) == null)
        n.assign(visited, new HashSet());      // backward search

    return new HashSet(visited.values());
  }

  void State.visit(Map<State, Set> visited, LinkedList<State> locked) {
    visited.put(this, null);
    for (Transition t : getOutgoingList())
      if (!visited.containsKey(t.getTo()))
        t.getTo().visit(visited, locked);
    locked.addFirst(this);
  }

  void State.assign(Map<State, Set> visited, Set root) {
    root.add(this);
    visited.put(this, root);
    for (Transition t : getIncomingList())
      if (visited.get(t.getFrom()) == null)
        t.getFrom().assign(visited, root);
  }

}
