aspect Analysis {

  /** Compute all states reachable from the current state */
  syn Set<State> State.reachable() circular [new HashSet<State>()] {
    Set<State> result = new HashSet<>();
    result.addAll(successors());
    for (State s : successors()) {
      result.addAll(s.reachable());
    }
    return result;
  }

  /** Compute the minimum number of transitions to the other state, ignoring states with empty labels */
  syn int State.minDistTo(State other) circular [-1] {
    if (this == other) {
      return 0;
    }
    int result = -1;
    for (Transition t : this.getOutgoingList()) {
      int dist = t.getTo().minDistTo(other);
      int delta = t.getLabel().isEmpty() ? 0 : 1;
      if (dist != -1 && (result == -1 || result > dist + delta)) {
        result = dist + delta;
      }
    }
    return result;
  }

  /** A transition is an epsilon transition if the label is empty */
  syn boolean Transition.isEpsilon() = getLabel().isEmpty();

  /** Collect all epsilon transitions */
  coll Set<Transition> StateMachine.epsilonTransitions() [new HashSet<>()];
  Transition contributes this when isEpsilon() to StateMachine.epsilonTransitions();

}
