package de.tudresden.inf.st.statemachine;

import beaver.Parser;
import de.tudresden.inf.st.statemachine.jastadd.model.StateMachine;
import de.tudresden.inf.st.statemachine.jastadd.parser.StateMachineParser;
import de.tudresden.inf.st.statemachine.jastadd.scanner.StateMachineScanner;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Utility methods for loading statemachine files.
 *
 * @author rschoene - Initial contribution
 */
public class ParserUtils {
  static StateMachine load(Path path) throws IOException, Parser.Exception {
    Reader reader = Files.newBufferedReader(path);
    StateMachineScanner scanner = new StateMachineScanner(reader);
    StateMachineParser parser = new StateMachineParser();
    StateMachine result = (StateMachine) parser.parse(scanner);
    reader.close();
    return result;
  }
}
